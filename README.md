
Laravel MailChimp Routes
========================

The goal of this Laravel package is to provide a set of endpoints to
communicate with the MailChimp API using Laravel routes.

A single-page application is a good use case for this package as it
allows you to communicate with the MailChimp API via Ajax and not
worry about cross-origin issues.

Todo
----

* Actually use HTTP POST parameters
* More error handling and API response checking
* Account for differences between the MailChimp API methods and the MailChimp PHP methods
