<?php

use Elite50\Mailchimp\Facades\MailchimpWrapper;

Route::post('/elite50/mailchimp/{topic?}/{method?}', function($topic = null, $method = null)
{
  
  /**
   * Set up the default JSON response
   *
   * We are assuming the worst
   */

  $response = array(

    'success'   => false,
    'status'    => 404,
    'messages'  => array(),
    'timestamp' => null,
    'data'      => array(),

  );


  /**
   * Make sure both a topic (e.g., campaigns, lists, etc.)
   * and method (e.g., abuse-reports) are provided
   */

  if (null === $topic || null === $method)
  {

    $response['messages'][] = array(

      'type'    => 'error',
      'message' => 'A MailChimp endpoint must be provided.',

    );

  }

  else
  {

    try 
    {

      $data = MailchimpWrapper::$topic($method, Input::all());

      /**
       * The topic endpoint (e.g., campaigns, lists, etc.) does
       * not exist
       */

      if (null === $data)
      {

        $response['messages'][] = array(

          'type'    => 'error',
          'message' => "The MailChimp API does not support a topic named {$topic}",

        );

      }
      
      else {

        $response['status']  = 200;
        $response['data']    = isset($data['data']) ? $data['data'] : $data;
        $response['success'] = true;

      }

    } 

    catch (Exception $e) 
    {

      $response['messages'][] = array(

        'type'    => 'error',
        'message' => $e->getMessage(),

      );

    }

  }


  /**
   * Set the Unix timestamp
   */

  $response['timestamp'] = time();


  /**
   * Return the JSON
   */

  return Response::json($response);

});
