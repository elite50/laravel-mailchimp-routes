<?php

namespace Elite50\Mailchimp;

use Mailchimp;

class MailchimpWrapper
{

  protected $mc;


  public function __construct(Mailchimp $mc)
  {

    $this->mc = $mc;

  }


  public function __call($topic, $args)
  {

    $topic   = $this->normalize($topic);
    $method  = $this->normalize($args[0]);
    $request = $args[1];

    if (
      property_exists($this->mc, $topic) &&
      method_exists($this->mc->{$topic}, $method)
    )
    {

      return call_user_func_array(
        array($this->mc->{$topic}, $method), $request
      );

    }

    return null;

  }


  protected function normalize($param)
  {

    $split  = array_map('ucfirst', explode('-', $param));

    $normal = lcfirst(implode('', $split));

    if ('list' == $normal) 
    {

      return 'getList';

    }

    return $normal;

  }

}
