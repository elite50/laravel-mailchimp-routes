<?php

namespace Elite50\Mailchimp\Facades;

use Illuminate\Support\Facades\Facade;

class MailchimpWrapper extends Facade
{

  protected static function getFacadeAccessor()
  {

    return 'mailchimp_wrapper';

  }

}
