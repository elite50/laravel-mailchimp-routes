
Configuration
=============

Out of paranoia that an API key would be committed, I ignored the
configuration file.

It's really simple, however. Here is all you need to do:

1. Create a file `config.php` in this directory.
2. Place the following code into newly created `config.php`:

__`config.php`__

```
#!php

<?php

return array(

  /**
   * Secret API key provided by MailChimp
   */

  'apikey' => 'your-secret-key',

);

```